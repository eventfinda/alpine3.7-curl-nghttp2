# README #

**DEPRECATED - Alpine (edge) now builds curl with HTTP/2 support**

Use: ```apk add --no-cache curl --repository http://dl-cdn.alpinelinux.org/alpine/edge/main/```

Alpine 3.8+ will include this build.

============================================

Docker repo to build an image with Curl + HTTP/2 support needed for Eventfinda's
infrastructure.

This image is built automatically with Docker Hub and used for copying the Curl
library and dependant files into the main images.

### Build and Run ##

* Build the image:  
  `$ docker build -t curl-nghttp2 .`
* Run the image:  
  `$ docker run --rm curl-nghttp2`